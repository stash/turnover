﻿using System;
using System.Collections.Generic;
using SimpleInjector;
using System.Reflection;
using MoreLinq;
using Turnover.Command.Contract;
using Turnover.Command.Contract.GenericCommands;
using Turnover.Command.Implementation.Decorators;
using Turnover.Command.Implementation.GenericCommands;
using Turnover.DataContract.Contract;
using Turnover.DataContract.Entities;

namespace Turnover.Command.Implementation
{
    public static class CommandInjector
    {
        public static void Register(Container container)
        {
            RegisterHandlersWithDecorators(container);
        }

        private static void RegisterHandlersWithDecorators(Container container)
        {
            // Go look in all assemblies and register all implementations
            // of ICommandHandler<T> by their closed interface:
            container.Register(
                typeof (ICommandHandler<>),
                new[] {typeof (ICommandHandler<>).GetTypeInfo().Assembly});

            // Decorate each returned ICommandHandler<T> object with
            // a TransactionCommandHandlerDecorator<T>.
            container.RegisterDecorator(
                typeof (ICommandHandler<>),
                typeof (TransactionCommandHandlerDecorator<>));

            // Decorate each returned ICommandHandler<T> object with
            // a DeadlockRetryCommandHandlerDecorator<T>.
            container.RegisterDecorator(
                typeof (ICommandHandler<>),
                typeof (DeadlockRetryCommandHandlerDecorator<>));
        }

        public static void RegisterGenericCommandHandlers(Container container)
        {
            var registerGenericHandlersMethod = typeof(CommandInjector).GetMethod(nameof(RegisterGenericHandlers));

            EntityTypesRepository
                .Entities
                .ForEach(entityType =>
            {
                if (!typeof(IEntity).IsAssignableFrom(entityType))
                {
                    var message = $"Entity {entityType.Name} doesn't implement IEntity";
                    throw new ArgumentException(message);
                }

                MethodInfo methodInfo = registerGenericHandlersMethod.MakeGenericMethod(entityType);
                methodInfo.Invoke(null, new[] { container });
            });
        }

        public static void RegisterGenericHandlers<TEntity>(Container container) where TEntity : class, IEntity, new()
        {
            container.Register(typeof(ICommandHandler<IGenericCreateCommand<TEntity>>), typeof(GenericCreateCommandHandler<TEntity>));
            container.Register(typeof(ICommandHandler<IGenericUpdateCommand<TEntity>>), typeof(GenericUpdateCommandHandler<TEntity>));
            container.Register(typeof(ICommandHandler<IGenericDeleteCommand<TEntity>>), typeof(GenericDeleteCommandHandler<TEntity>));
        }
    }
}
