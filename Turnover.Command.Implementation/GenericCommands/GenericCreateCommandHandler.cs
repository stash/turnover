﻿using System;
using Turnover.Command.Contract;
using Turnover.Command.Contract.GenericCommands;
using Turnover.Common;
using Turnover.Common.SequentialGuid;
using Turnover.DataContract.Contract;
using Turnover.EntityFramework;

namespace Turnover.Command.Implementation.GenericCommands
{
    public class GenericCreateCommandHandler<TEntity>: ICommandHandler<IGenericCreateCommand<TEntity>> where TEntity: class, IEntity
    {
        private readonly TurnoverDbContext _context;

        public GenericCreateCommandHandler(TurnoverDbContext context)
        {
            _context = context;
        }

        public void Handle(IGenericCreateCommand<TEntity> command)
        {
            if (command.Entity.Id == Guid.Empty)
            {
                command.Entity.Id = SequentialGuidGenerator.NewSqlServerGuid();
            }

            _context.Set<TEntity>().Add(command.Entity);
        }
    }
}