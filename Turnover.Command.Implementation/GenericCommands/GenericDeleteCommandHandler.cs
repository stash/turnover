﻿using System.Data.Entity;
using Turnover.Command.Contract;
using Turnover.Command.Contract.GenericCommands;
using Turnover.DataContract.Contract;
using Turnover.EntityFramework;

namespace Turnover.Command.Implementation.GenericCommands
{
    public class GenericDeleteCommandHandler<TEntity> : ICommandHandler<IGenericDeleteCommand<TEntity>> where TEntity : class, IEntity, new()
    {
        private readonly TurnoverDbContext _context;

        public GenericDeleteCommandHandler(TurnoverDbContext context)
        {
            _context = context;
        }

        public void Handle(IGenericDeleteCommand<TEntity> command)
        {
            var entity = new TEntity()
            {
                Id = command.Id
            };
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Deleted;
        }
    }
}