using System;
using System.Collections.Generic;
using System.Linq;
using Turnover.DataContract.Entities;
using Turnover.WebApi.Controllers;

namespace Turnover.WebApi
{
    internal class GenericApiControllersRegistration : IControllersRegistration
    {
        private readonly Lazy<IReadOnlyCollection<ControllerRegistration>> _registrations;

        public IEnumerable<ControllerRegistration> Registrations => _registrations.Value;

        public GenericApiControllersRegistration()
        {
            _registrations = new Lazy<IReadOnlyCollection<ControllerRegistration>>(GetRegistrations);
        }

        private IReadOnlyCollection<ControllerRegistration> GetRegistrations()
        {
            return EntityTypesRepository
                .Entities
                .Select(entityType => NewControllerRegistration(entityType.Name, entityType))
                .ToArray();
        }
        
        private static ControllerRegistration NewControllerRegistration(string controllerName, Type entityType)
        {
            var result =
                new ControllerRegistration
                {
                    ControllerType = typeof(GenericController<>).MakeGenericType(entityType),
                    ControllerName = controllerName,
                    EntityType = entityType
                };
            return result;
        }
    }
}