﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using Turnover.Common;

namespace Turnover.WebApi
{
    internal class ApiControllerSelector : IApiControllerSelector
    {
        #region private fields and utils

        private readonly IEnumerable<IControllersRegistration> _controllersRegistrations;
        private Lazy<IDictionary<string, HttpControllerDescriptor>> _controllerNameMapping;
        private Lazy<IDictionary<Type, ControllerRegistration>> _controllersRegistration;

        private IEnumerable<ControllerRegistration> SelectRegistrations()
        {
            var result = _controllersRegistrations
                .SelectMany(r => r.Registrations.SelectIfAny());
            return result;
        }

        private IDictionary<string, HttpControllerDescriptor> CreateNameMapping()
        {
            var result = SelectRegistrations()
                .ToDictionary(
                    mapper => mapper.ControllerName,
                    mapper =>
                    new HttpControllerDescriptor
                        (
                        GlobalConfiguration.Configuration,
                        mapper.ControllerName,
                        mapper.ControllerType
                        ),
                    StringComparer.InvariantCultureIgnoreCase
                );

            return result;
        }

        private IDictionary<Type, ControllerRegistration> CreateRegistrations()
        {
            var result = SelectRegistrations()
                .ToDictionary
                (
                    r => r.ControllerType,
                    r => r
                );
            return result;
        }

        #endregion

        #region dependency contract

        public ApiControllerSelector(IEnumerable<IControllersRegistration> controllersRegistrations)
        {
            _controllersRegistrations = controllersRegistrations;
            _controllerNameMapping = new Lazy<IDictionary<string, HttpControllerDescriptor>>(CreateNameMapping);
            _controllersRegistration = new Lazy<IDictionary<Type, ControllerRegistration>>(CreateRegistrations);
        }

        #endregion

        #region Implementation of IHttpControllerSelector

        public HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            var controllerKey = ControllerName.Suffix.ToLowerInvariant();
            if (request.GetRouteData().Values.ContainsKey(controllerKey))
            {
                var key = request.GetRouteData().Values[controllerKey] as string;
                if (key != null)
                {
                    HttpControllerDescriptor controller;
                    if (_controllerNameMapping.Value.TryGetValue(key, out controller))
                    {
                        return controller;
                    }
                    var message = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content =
                                new StringContent(string.Format("Controller for the entity '{0}' wasn't found", key))
                    };
                    throw new HttpResponseException(message);
                }
            }

            var exceptionMessage = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent("It isn't possible to select controller: Invalid URL format")
            };
            throw new HttpResponseException(exceptionMessage);
        }

        public IDictionary<string, HttpControllerDescriptor> GetControllerMapping()
        {
            return _controllerNameMapping.Value;
        }

        #endregion

        public IDictionary<Type, ControllerRegistration> ControllersRegistration
        {
            get { return _controllersRegistration.Value; }
        }
    }
}