using System.Collections.Generic;

namespace Turnover.WebApi
{
    public class ControllersRegistration : IControllersRegistration
    {
        public IEnumerable<ControllerRegistration> Registrations { get; set; }
    }
}