using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;

namespace Turnover.WebApi
{
    public class CompositionRoot : IHttpControllerActivator
    {
        private readonly Container _container;

        public CompositionRoot(Container container)
        {
            _container = container;
        }

        public IHttpController Create(HttpRequestMessage request,
            HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            request.RegisterForDispose(_container.BeginExecutionContextScope());

            return (IHttpController)_container.GetInstance(controllerType);
        }
    }
}