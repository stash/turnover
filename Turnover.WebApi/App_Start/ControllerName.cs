﻿using System;

namespace Turnover.WebApi
{
    public static class ControllerName
    {
        public const string Suffix = "Controller";

        public static string GetShortName(this Type type)
        {
            var controllerTypeName = type.Name;

            const string suffix = Suffix;
            if (controllerTypeName.EndsWith(suffix))
            {
                return controllerTypeName.Substring(0, controllerTypeName.Length - suffix.Length);
            }

            return controllerTypeName;
        }

        public static string GenerateForType(Type entityType)
        {
            return $"{entityType.Name}{Suffix}";
        }
    }
}