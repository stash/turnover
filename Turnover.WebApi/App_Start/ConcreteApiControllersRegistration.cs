using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Turnover.WebApi.Controllers;

namespace Turnover.WebApi
{
    internal class ConcreteApiControllersRegistration : IControllersRegistration
    {
        private readonly Lazy<IReadOnlyCollection<ControllerRegistration>> _registrations;

        public IEnumerable<ControllerRegistration> Registrations => _registrations.Value;

        public ConcreteApiControllersRegistration()
        {
            _registrations = new Lazy<IReadOnlyCollection<ControllerRegistration>>(GetRegistrations);
        }

        private IReadOnlyCollection<ControllerRegistration> GetRegistrations()
        {
            return SelectRegistrations().ToArray();
        }

        private static ControllerRegistration NewControllerRegistration(Type controllerType)
        {
            var result =
                new ControllerRegistration
                {
                    ControllerType = controllerType,
                    ControllerName = controllerType.GetShortName(),
                    EntityType = null
                };
            return result;
        }

        private IEnumerable<ControllerRegistration> SelectRegistrations()
        {
            var genericControllerType = typeof(GenericController<>);

            var controllerTypes = genericControllerType
                .Assembly
                .GetExportedTypes()
                .Where(type => typeof(ApiController).IsAssignableFrom(type) && type != genericControllerType);

            return controllerTypes.Select(NewControllerRegistration);
        }
    }
}