﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dispatcher;

namespace Turnover.WebApi
{
    public interface IApiControllerSelector : IHttpControllerSelector
    {
        IDictionary<Type, ControllerRegistration> ControllersRegistration { get; }
    }
}