using System.Collections.Generic;

namespace Turnover.WebApi
{
    public interface IControllersRegistration
    {
        IEnumerable<ControllerRegistration> Registrations { get; }
    }
}