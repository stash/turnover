﻿using System;

namespace Turnover.WebApi
{
    public class ControllerRegistration
    {
        public Type EntityType { get; set; }
        public Type ControllerType { get; set; }
        public string ControllerName { get; set; }
    }
}