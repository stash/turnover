﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using Turnover.Command.Implementation;
using Turnover.EntityFramework;
using Turnover.Metadata.Contract;
using Turnover.Metadata.Implementation;
using Turnover.Query.Implementation;

namespace Turnover.WebApi
{
    public static class DiContainerConfig
    {
        public static void Initialize()
        {
            var config = GlobalConfiguration.Configuration;

            // Simple Injector set up
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
            
            RegisterExternal(container);

            CommandInjector.RegisterGenericCommandHandlers(container);

            container.Verify();

            config.Services.Replace(
                typeof (IHttpControllerActivator),
                new CompositionRoot(container));

            var controllerRegistrations = new List<IControllersRegistration>
            {
                new GenericApiControllersRegistration(),
                new ConcreteApiControllersRegistration()
            };
            config.Services.Replace(typeof (IHttpControllerSelector), new ApiControllerSelector(controllerRegistrations));
        }

        private static void RegisterExternal(Container container)
        {
            // Register your stuff here
            container.Register<IMetadataProvider, MetadataProvider>();
            EntityFrameworkInjector.Register(container);
            CommandInjector.Register(container);
            QueryInjector.Register(container);
        }
    }
}