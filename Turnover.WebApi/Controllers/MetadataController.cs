﻿using System.Collections.Generic;
using System.Web.Http;
using Turnover.Metadata.Contract;

namespace Turnover.WebApi.Controllers
{
    public class MetadataController : ApiController
    {
        private IMetadataProvider _metadataProvider;

        public MetadataController(IMetadataProvider metadataProvider)
        {
            _metadataProvider = metadataProvider;
        }

        public IDictionary<string, EntityMetadata> Get()
        {
            return _metadataProvider.Metdata;
        }
    }
}