﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Turnover.Command.Contract;
using Turnover.Command.Contract.GenericCommands;
using Turnover.Command.Implementation.GenericCommands;
using Turnover.DataContract.Contract;
using Turnover.Query.Contract;

namespace Turnover.WebApi.Controllers
{
    public class GenericController<TEntity> : ApiController where TEntity : class, IEntity
    {
        private readonly IQueryableDatabase _database;
        private readonly ICommandHandler<IGenericCreateCommand<TEntity>> _createCommandHandler;
        private readonly ICommandHandler<IGenericUpdateCommand<TEntity>> _updateCommandHandler;
        private readonly ICommandHandler<IGenericDeleteCommand<TEntity>> _deleteCommandHandler;

        public GenericController(IQueryableDatabase database,
            ICommandHandler<IGenericCreateCommand<TEntity>> createCommandHandler,
            ICommandHandler<IGenericUpdateCommand<TEntity>> updateCommandHandler,
            ICommandHandler<IGenericDeleteCommand<TEntity>> deleteCommandHandler)
        {
            _database = database;
            _createCommandHandler = createCommandHandler;
            _updateCommandHandler = updateCommandHandler;
            _deleteCommandHandler = deleteCommandHandler;
        }

        public async Task<IEnumerable<TEntity>> Get()
        {
            List<TEntity> entities = await _database.Query<TEntity>().Take(10).ToListAsync();
            var entitiesToReturn = entities.Select(Mapper.Map<TEntity>);
            return entitiesToReturn;
        }

        public TEntity Get(Guid id)
        {
            TEntity entity = _database.QueryWithCollections<TEntity>().FirstOrDefault(product => product.Id == id);
            var entityToReturn = Mapper.Map<TEntity>(entity);
            return entityToReturn;
        }

        public void Post(TEntity entity)
        {
            _createCommandHandler.Handle(new GenericCreateCommand<TEntity>(entity));
        }

        public void Put(TEntity entity)
        {
            _updateCommandHandler.Handle(new GenericUpdateCommand<TEntity>(entity));
        }

        public void Delete(Guid id)
        {
            _deleteCommandHandler.Handle(new GenericDeleteCommand<TEntity>(id));
        }
    }
}
