﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Turnover.Common;
using Turnover.DataContract.Contract;
using Turnover.DataContract.Entities;
using Turnover.Metadata.Contract;

namespace Turnover.Metadata.Implementation
{
    public class MetadataProvider : IMetadataProvider
    {
        private readonly Lazy<Dictionary<string, EntityMetadata>> _metadata;

        public IDictionary<string, EntityMetadata> Metdata => _metadata.Value;

        public MetadataProvider()
        {
            _metadata = new Lazy<Dictionary<string, EntityMetadata>>(GenerateMetadata);
        }

        private Dictionary<string, EntityMetadata> GenerateMetadata()
        {
            var metadata = EntityTypesRepository.Entities.ToDictionary(type => type.Name, type =>
            {
                var isAggregateRoot = typeof (IAggregateRoot).IsAssignableFrom(type);

                var allProperties = type
                    .GetProperties()
                    .Where(property => property.PropertyType != typeof(Guid));

                var properties = allProperties
                    .GroupBy(property => IsCollectionType(property.PropertyType))
                    .ToList();

                var fields = properties
                    .Take(1)
                    .SelectMany(src => src)
                    .Select(CreateFieldMetadata)
                    .ToList();

                var collections = properties
                    .Skip(1)
                    .Take(1)
                    .SelectMany(src => src)
                    .Select(CreateCollectionMetadata)
                    .ToList();

                return new EntityMetadata(type.Name, isAggregateRoot, fields, collections);
            });

            return metadata;
        }

        private static bool IsCollectionType(Type propertyType)
        {
            if (propertyType == typeof(string))
            {
                return false;
            }

            var result = (typeof(IEnumerable).IsAssignableFrom(propertyType));
            return result;
        }

        private static FieldMetadata CreateFieldMetadata(PropertyInfo property)
        {
            return new FieldMetadata
            {
                Name = property.Name,
                Type = property.PropertyType.Name,
                Label = property.Name.SplitCamelCase(),
                Required = false,
                MinLength = 0,
                MaxLength = 1000
            };
        }

        private static CollectionMetadata CreateCollectionMetadata(PropertyInfo property)
        {
            var typeName = property.PropertyType.GetGenericArguments()[0].Name;

            return new CollectionMetadata
            {
                Name = property.Name,
                Label = property.Name.SplitCamelCase(),
                TypeName = typeName,
                TypeLabel = typeName.SplitCamelCase(),
                Required = false,
                MinLength = 0,
                MaxLength = 1000
            };
        }
    }
}
