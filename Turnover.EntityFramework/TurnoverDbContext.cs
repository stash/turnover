﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Turnover.DataContract.Entities;
using Turnover.DataContract.Entities.Entities;

namespace Turnover.EntityFramework
{
    public class TurnoverDbContext : DbContext
    {
        public TurnoverDbContext() : base("TurnoverConnectionString")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.AddBefore<ForeignKeyIndexConvention>(new NoUnderscoreForeignKeyNamingConvention());
        }

        public DbSet<Address> Address { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<EmailAddress> EmailAddress { get; set; }
        public DbSet<Fee> Fee { get; set; }
        public DbSet<FeeType> FeeType { get; set; }
        public DbSet<PhoneNumber> PhoneNumber { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<PurchaseOrder> Purchase { get; set; }
        public DbSet<Shipment> Shipment { get; set; }
        public DbSet<Warehouse> Warehouse { get; set; }
    }
}
