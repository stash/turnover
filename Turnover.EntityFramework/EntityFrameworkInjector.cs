﻿using SimpleInjector;

namespace Turnover.EntityFramework
{
    public static class EntityFrameworkInjector
    {
        public static void Register(Container container)
        {
            container.Register<TurnoverDbContext>(Lifestyle.Scoped);
        }
    }
}
