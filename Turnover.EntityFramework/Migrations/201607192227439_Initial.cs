namespace Turnover.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ContactId = c.Guid(nullable: false),
                        City = c.String(),
                        AddressLine1 = c.String(),
                        MainIndex = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmailAddresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ContactId = c.Guid(nullable: false),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.PhoneNumbers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ContactId = c.Guid(nullable: false),
                        CountryCode = c.String(),
                        OperatorCode = c.String(),
                        Number = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.Grers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PhoneNumberId = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PhoneNumbers", t => t.PhoneNumberId, cascadeDelete: true)
                .Index(t => t.PhoneNumberId);
            
            CreateTable(
                "dbo.Fees",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FeeTypeId = c.Guid(nullable: false),
                        PurchaseId = c.Guid(),
                        SaleId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseId)
                .ForeignKey("dbo.SaleOrders", t => t.SaleId)
                .Index(t => t.PurchaseId)
                .Index(t => t.SaleId);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        ShipmentId = c.Guid(),
                        SupplierId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Shipments", t => t.ShipmentId)
                .ForeignKey("dbo.Contacts", t => t.SupplierId)
                .Index(t => t.ShipmentId)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.PurchaseOrderDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PurchaseOrderId = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderId, cascadeDelete: true)
                .Index(t => t.PurchaseOrderId);
            
            CreateTable(
                "dbo.Shipments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Sent = c.DateTime(nullable: false),
                        Arrived = c.DateTime(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BrokerageFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SaleOrders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        ClientId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SaleOrderDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SaleOrderId = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SaleOrders", t => t.SaleOrderId, cascadeDelete: true)
                .Index(t => t.SaleOrderId);
            
            CreateTable(
                "dbo.FeeTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        ProductCode = c.String(),
                        CategoryId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Warehouses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Fees", "SaleId", "dbo.SaleOrders");
            DropForeignKey("dbo.SaleOrderDetails", "SaleOrderId", "dbo.SaleOrders");
            DropForeignKey("dbo.PurchaseOrders", "SupplierId", "dbo.Contacts");
            DropForeignKey("dbo.PurchaseOrders", "ShipmentId", "dbo.Shipments");
            DropForeignKey("dbo.Fees", "PurchaseId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderDetails", "PurchaseOrderId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PhoneNumbers", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Grers", "PhoneNumberId", "dbo.PhoneNumbers");
            DropForeignKey("dbo.EmailAddresses", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Addresses", "ContactId", "dbo.Contacts");
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.SaleOrderDetails", new[] { "SaleOrderId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "PurchaseOrderId" });
            DropIndex("dbo.PurchaseOrders", new[] { "SupplierId" });
            DropIndex("dbo.PurchaseOrders", new[] { "ShipmentId" });
            DropIndex("dbo.Fees", new[] { "SaleId" });
            DropIndex("dbo.Fees", new[] { "PurchaseId" });
            DropIndex("dbo.Grers", new[] { "PhoneNumberId" });
            DropIndex("dbo.PhoneNumbers", new[] { "ContactId" });
            DropIndex("dbo.EmailAddresses", new[] { "ContactId" });
            DropIndex("dbo.Addresses", new[] { "ContactId" });
            DropTable("dbo.Warehouses");
            DropTable("dbo.Products");
            DropTable("dbo.FeeTypes");
            DropTable("dbo.SaleOrderDetails");
            DropTable("dbo.SaleOrders");
            DropTable("dbo.Shipments");
            DropTable("dbo.PurchaseOrderDetails");
            DropTable("dbo.PurchaseOrders");
            DropTable("dbo.Fees");
            DropTable("dbo.Grers");
            DropTable("dbo.PhoneNumbers");
            DropTable("dbo.EmailAddresses");
            DropTable("dbo.Contacts");
            DropTable("dbo.Categories");
            DropTable("dbo.Addresses");
        }
    }
}
