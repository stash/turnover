using System.Collections.Generic;
using System.Data.Entity.Migrations;
using Turnover.Common.SequentialGuid;
using Turnover.DataContract.Entities;
using Turnover.DataContract.Entities.Entities;

namespace Turnover.EntityFramework.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<TurnoverDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TurnoverDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. 
            var contactIdOne = SequentialGuidGenerator.NewSqlServerGuid();

            context.Contact.AddOrUpdate(
                p => p.Id,
                new Contact
                {
                    Id = contactIdOne,
                    FirstName = "Andrew",
                    LastName = "Peters",
                    Type = ContactType.Client,
                    Emails = new List<EmailAddress>
                    {
                        new EmailAddress
                        {
                            Id = SequentialGuidGenerator.NewSqlServerGuid(),
                            Address = "andrew.piters555@gmail.com"
                        },
                        new EmailAddress
                        {
                            Id = SequentialGuidGenerator.NewSqlServerGuid(),
                            Address = "backup.email@gmail.com"
                        }
                    },
                    PhoneNumbers = new List<PhoneNumber>
                    {
                        new PhoneNumber
                        {
                            Id = SequentialGuidGenerator.NewSqlServerGuid(),
                            CountryCode = "380",
                            OperatorCode = "68",
                            Number = "9998877",
                            Grers = new List<Grer>
                            {
                                new Grer
                                {
                                    Id = SequentialGuidGenerator.NewSqlServerGuid(),
                                    Name = "dsfwerwe"
                                },
                                new Grer
                                {
                                    Id = SequentialGuidGenerator.NewSqlServerGuid(),
                                    Name = "Deewr"
                                }
                            }
                        },
                        new PhoneNumber
                        {
                            Id = SequentialGuidGenerator.NewSqlServerGuid(),
                            CountryCode = "380",
                            OperatorCode = "97",
                            Number = "1118877",
                            Grers = new List<Grer>
                            {
                                new Grer
                                {
                                    Id = SequentialGuidGenerator.NewSqlServerGuid(),
                                    Name = "Derewr"
                                }
                            }
                        }
                    },
                    Addresses = new List<Address>
                    {
                        new Address
                        {
                            Id = SequentialGuidGenerator.NewSqlServerGuid(),
                            City = "Kharkov",
                            AddressLine1 = "Some street 1, flat 2",
                            MainIndex = "31106"
                        }
                    }
                },
                new Contact {
                    Id = SequentialGuidGenerator.NewSqlServerGuid(),
                    FirstName = "Brice",
                    LastName = "Lambson",
                    Type = ContactType.Partner,
                },
                new Contact
                {
                    Id = SequentialGuidGenerator.NewSqlServerGuid(),
                    FirstName = "Rowan",
                    LastName = "Miller",
                    Type = ContactType.Supplier
                }
            );
        }
    }
}