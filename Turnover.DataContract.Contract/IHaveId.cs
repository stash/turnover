﻿using System;

namespace Turnover.DataContract.Contract
{
    public interface IHaveId
    {
        Guid Id { get; set; }
    }
}