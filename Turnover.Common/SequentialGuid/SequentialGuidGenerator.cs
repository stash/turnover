﻿using System;
using System.Security.Cryptography;
using System.Threading;

namespace Turnover.Common.SequentialGuid
{
    /// <summary>
    /// http://csharptest.net/1250/why-guid-primary-keys-are-a-databases-worst-nightmare/
    /// http://www.codeproject.com/Articles/388157/GUIDs-as-fast-primary-keys-under-multiple-database
    /// </summary>
    public static class SequentialGuidGenerator
    {
        static int _sequenced = (int)DateTime.UtcNow.Ticks;
        
        private static readonly RNGCryptoServiceProvider _randomProvider = new RNGCryptoServiceProvider();
        
        private static readonly byte[] _randomBytes = new byte[10];

        public static Guid NewSqlServerGuid()
        {
            long ticks = DateTime.UtcNow.Ticks;
            int sequenceNum = Interlocked.Increment(ref _sequenced);
            lock (_randomBytes)
            {
                _randomProvider.GetBytes(_randomBytes);

                byte[] guidBytes = new byte[16];

                Buffer.BlockCopy(_randomBytes, 0, guidBytes, 0, 10);
                guidBytes[10] = (byte)(ticks >> 32);
                guidBytes[11] = (byte)(ticks >> 16);
                guidBytes[12] = (byte)(ticks >> 8);
                guidBytes[13] = (byte)ticks;
                guidBytes[14] = (byte)(sequenceNum >> 8);
                guidBytes[15] = (byte) sequenceNum;

                return new Guid(guidBytes);
            }
        }
    }
}
