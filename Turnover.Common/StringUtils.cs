﻿using System.Text.RegularExpressions;

namespace Turnover.Common
{
    public static class StringUtils
    {
        public static string SplitCamelCase(this string input)
        {
            return Regex.Replace(input, "([A-Z])", " $1", RegexOptions.Compiled).Trim();
        }
    }
}