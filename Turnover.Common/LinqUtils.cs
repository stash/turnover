﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Turnover.Common
{
    public static class LinqUtils
    {
        private static IEnumerable<T> RestartFromCurrent<T>(IEnumerator<T> enumerator)
        {
            try
            {
                yield return enumerator.Current;
                while (enumerator.MoveNext())
                {
                    yield return enumerator.Current;
                }
            }
            finally
            {
                enumerator.Dispose();
            }
        }

        public static bool IfAny<T>(this IEnumerable<T> src, Action<IEnumerable<T>> process)
        {
            return src
                .IfAny(
                    enumerable =>
                    {
                        process(enumerable);
                        return true;
                    },
                    false
                );
        }

        public static TResult IfAny<T, TResult>(this IEnumerable<T> src, Func<IEnumerable<T>, TResult> func,
                                                TResult defaultIfEmpty)
        {
            return IfAny(src, func, () => defaultIfEmpty);
        }

        public static TResult IfAny<T, TResult>(this IEnumerable<T> src,
                                                Func<IEnumerable<T>, TResult> func,
                                                Func<TResult> defaultFunc)
        {
            if (src == null)
                return defaultFunc();

            var enumerator = src.GetEnumerator();

            if (enumerator.MoveNext())
            {
                return func(RestartFromCurrent(enumerator));
            }
            enumerator.Dispose();
            return defaultFunc();
        }

        public static void ForEachIfAny<T>(this IEnumerable<T> src, Action<T> process)
        {
            src
                .IfAny(
                    s =>
                    {
                        foreach (var element in s)
                        {
                            process(element);
                        }
                    });
        }

        public static IEnumerable<T> SelectIfAny<T>(this IEnumerable<T> src)
        {
            return src ?? Enumerable.Empty<T>();
        }
    }
}
