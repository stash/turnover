﻿using System;
using Xunit;
using Turnover.Common.SequentialGuid;

namespace Turnover.Common.Test
{
    public class SequentialGuidGeneratorTests
    {
        [Fact]
        public void SequentialGuidGenerator_Returns_ValidGuid()
        {
            var guid = SequentialGuidGenerator.NewSqlServerGuid();

            Assert.NotNull(guid);
        }

        [Fact]
        public void SequentialGuidGenerator_Returns_SequentalGuids()
        {
            var guid1 = SequentialGuidGenerator.NewSqlServerGuid();
            var guid2 = SequentialGuidGenerator.NewSqlServerGuid();
            var guid3 = SequentialGuidGenerator.NewSqlServerGuid();

            var long1 = GetSortablePartOfGuid(guid1);
            var long2 = GetSortablePartOfGuid(guid2);
            var long3 = GetSortablePartOfGuid(guid3);

            var guidsAreSequental = long3 > long2 && long2 > long1;

            Assert.True(guidsAreSequental);
        }

        private long GetSortablePartOfGuid(Guid guid)
        {
            var guidBytes = guid.ToByteArray();

            var sortableInSqlServerBytes = new byte[8];

            Buffer.BlockCopy(guidBytes, 10, sortableInSqlServerBytes, 2, 6);

            return BitConverter.ToInt64(sortableInSqlServerBytes, 0);
        }
    }
}
