﻿using System;
using System.Collections.Generic;
using System.Linq;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities
{
    public static class EntityTypesRepository
    {
        private static readonly Lazy<List<Type>> _allEntities;

        static EntityTypesRepository()
        {
            _allEntities = new Lazy<List<Type>>(GetEntitiesToRegister);
        }

        public static List<Type> Entities => _allEntities.Value;

        private static List<Type> GetEntitiesToRegister()
        {
            var repositoryAssembly = typeof(EntityTypesRepository).Assembly;

            var entityTypes = repositoryAssembly
                .GetExportedTypes();

            return entityTypes.Where(entityType => typeof(IEntity).IsAssignableFrom(entityType)).ToList();
        }
    }
}
