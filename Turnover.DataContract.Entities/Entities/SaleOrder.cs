﻿using System;
using System.Collections.Generic;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class SaleOrder: IAggregateRoot
    {
        public Guid Id { get; set; }

        public DateTime OrderDate { get; set; }

        public Guid ClientId { get; set; }

        public ICollection<SaleOrderDetails> Details { get; set; }
    }
}