﻿using System;
using System.Collections.Generic;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class Contact : IAggregateRoot
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public ContactType Type { get; set; }

        public ICollection<PhoneNumber> PhoneNumbers { get; set; }

        public ICollection<EmailAddress> Emails { get; set; }

        public ICollection<Address> Addresses { get; set; }
    }
}
