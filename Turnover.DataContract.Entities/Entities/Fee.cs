﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class Fee : IEntity
    {
        public Guid Id { get; set; }

        public SaleOrder Sale { get; set; }

        public PurchaseOrder Purchase { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Amount { get; set; }

        public Guid FeeTypeId { get; set; }
    }
}
