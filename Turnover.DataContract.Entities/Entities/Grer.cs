using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    //This entity is useless - it is needed only as third layer of entities
    public class Grer : IEntity
    {
        public Guid Id { get; set; }

        public Guid PhoneNumberId { get; set; }

        public string Name { get; set; }
    }
}