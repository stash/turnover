﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class Address : IEntity
    {
        public Guid Id { get; set; }

        public Guid ContactId { get; set; }

        public string City { get; set; }

        public string AddressLine1 { get; set; }

        public string MainIndex { get; set; }
    }
}
