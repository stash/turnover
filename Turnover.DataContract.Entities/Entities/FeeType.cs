﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class FeeType : IEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
