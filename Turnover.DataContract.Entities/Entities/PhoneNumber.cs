﻿using System;
using System.Collections.Generic;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class PhoneNumber : IEntity
    {
        public Guid Id { get; set; }

        public Guid ContactId { get; set; }

        public string CountryCode { get; set; }

        public string OperatorCode { get; set; }

        public string Number { get; set; }

        public ICollection<Grer> Grers { get; set; }
    }
}
