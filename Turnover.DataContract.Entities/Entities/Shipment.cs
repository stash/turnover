﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class Shipment : IAggregateRoot
    {
        public Guid Id { get; set; }

        public DateTime Sent { get; set; }

        public DateTime Arrived { get; set; }

        public decimal Price { get; set; }

        public decimal BrokerageFee { get; set; }
    }
}
