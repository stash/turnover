﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class Item: IAggregateRoot
    {
        public Guid Id { get; set; }

        public Product Product { get; set; }

        public Warehouse Warehouse { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Serial { get; set; }
    }
}
