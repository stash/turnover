﻿using System;
using System.Collections.Generic;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class PurchaseOrder : IAggregateRoot
    {
        public Guid Id { get; set; }

        public DateTime OrderDate { get; set; }

        public Contact Supplier { get; set; }

        public Shipment Shipment { get; set; }

        public virtual ICollection<PurchaseOrderDetails> Details { get; set; }

        public virtual ICollection<Fee> Fees { get; set; }
    }
}
