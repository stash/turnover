﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class Category : IAggregateRoot
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
