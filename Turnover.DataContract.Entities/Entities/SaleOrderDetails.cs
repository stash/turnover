﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class SaleOrderDetails : IEntity
    {
        public Guid Id { get; set; }

        public Guid SaleOrderId { get; set; }

        public Guid ProductId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
