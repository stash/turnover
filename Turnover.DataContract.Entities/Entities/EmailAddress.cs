﻿using System;
using Turnover.DataContract.Contract;

namespace Turnover.DataContract.Entities.Entities
{
    public class EmailAddress : IEntity
    {
        public Guid Id { get; set; }

        public Guid ContactId { get; set; }

        public string Address { get; set; }
    }
}
