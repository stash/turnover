﻿using System;

namespace Turnover.DataContract.Entities
{
    [Flags]
    public enum ContactType
    {
        Unknown = 0,
        Client = 1,
        Supplier = 2,
        Partner = 4
    }
}