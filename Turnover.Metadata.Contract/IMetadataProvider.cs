using System.Collections.Generic;

namespace Turnover.Metadata.Contract
{
    public interface IMetadataProvider
    {
        IDictionary<string, EntityMetadata> Metdata { get; }
    }
}