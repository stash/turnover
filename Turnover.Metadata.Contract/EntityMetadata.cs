﻿using System.Collections.Generic;

namespace Turnover.Metadata.Contract
{
    public class EntityMetadata
    {
        public EntityMetadata(string name, bool isAggregateRoot, ICollection<FieldMetadata> fields, ICollection<CollectionMetadata> collections)
        {
            Name = name;
            IsAggregateRoot = isAggregateRoot;
            Fields = fields;
            Collections = collections;
        }

        public string Name { get; }
        public bool IsAggregateRoot { get; }
        public ICollection<FieldMetadata> Fields { get; }
        public ICollection<CollectionMetadata> Collections { get; }
    }
}