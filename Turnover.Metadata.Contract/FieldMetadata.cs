﻿namespace Turnover.Metadata.Contract
{
    public class FieldMetadata
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Label { get; set; }
        public bool Required { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
    }
}