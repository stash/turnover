namespace Turnover.Metadata.Contract
{
    public class CollectionMetadata
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string TypeName { get; set; }
        public string TypeLabel { get; set; }
        public bool Required { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
    }
}