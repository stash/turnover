﻿using System;
using System.Collections;
using System.Data.Entity;
using System.Linq;
using Turnover.EntityFramework;
using Turnover.Query.Contract;

namespace Turnover.Query.Implementation
{
    public class QueryableDatabase : IQueryableDatabase
    {
        private readonly TurnoverDbContext _context;

        public QueryableDatabase(TurnoverDbContext context)
        {
            _context = context;
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return _context.Set<T>();
        }

        public IQueryable<T> QueryWithCollections<T>() where T : class
        {
            var collections = typeof(T)
                .GetProperties()
                .Where(property => IsCollectionType(property.PropertyType));

            IQueryable<T> queryable = Query<T>();

            return collections.Aggregate(queryable, (current, collection) => current.Include(collection.Name));
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        private static bool IsCollectionType(Type propertyType)
        {
            if (propertyType == typeof(string))
            {
                return false;
            }

            var result = (typeof(IEnumerable).IsAssignableFrom(propertyType));
            return result;
        }
    }
}
