﻿using SimpleInjector;
using Turnover.Query.Contract;

namespace Turnover.Query.Implementation
{
    public static class QueryInjector
    {
        public static void Register(Container container)
        {
            container.Register<IQueryableDatabase, QueryableDatabase>(Lifestyle.Scoped);
        }
    }
}
