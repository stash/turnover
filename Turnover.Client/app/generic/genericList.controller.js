﻿(function () {
    'use strict';
    var controllerId = 'genericListController';
    angular.module('app')
        .controller(controllerId, ['$scope', '$http', '$state', 'common', 'apiHost', 'metadata', GenericListController]);

    function GenericListController($scope, $http, $state, common, apiHost, metadata) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        
        vm.gridOptions = {
            columnDefs: [],
            rowData: []
        };

        var entityName = $state.current.params.entityName;

        var apiUrl = apiHost + entityName;
        vm.entityName = entityName;
        vm.title = entityName;

        vm.addNew = function () {
            $state.transitionTo(entityName + '.add');
        }

        function rowClickedEventListener (gridContext) {

            var params = {};
            params[entityName + 'Id'] = gridContext.data.Id;

            $state.transitionTo(entityName + '.one.edit', params);
        } 

        function initTable() {
            $http
            .get(apiUrl)
            .then(function (response) {

                var entities = response.data;

                var fields = metadata[entityName].Fields;

                var colDefs = fields.map(function (fieldMetadata) {
                    return {
                        headerName: fieldMetadata.Label,
                        field: fieldMetadata.Name
                    }
                });
                vm.gridOptions.api.setColumnDefs(colDefs);

                vm.gridOptions.api.setRowData(entities);

                vm.gridOptions.api.addEventListener('rowClicked', rowClickedEventListener);
            });
        }
        
        activate();

        function activate() {
            initTable();
            common.activateController([], controllerId)
                .then(function () { log('Activated ' + entityName + ' View'); });
        }
    }
})();