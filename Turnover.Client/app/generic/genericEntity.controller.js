﻿(function() {
    'use strict';
    var controllerId = 'genericEntityController';
    angular.module('app')
        .controller(controllerId, ['$scope', '$http', '$state', '$stateParams', 'common', 'apiHost', 'metadata', 'formlyMetadata', GenericEntityController]);

    function GenericEntityController($scope, $http, $state, $stateParams, common, apiHost, metadata, formlyMetadata) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        $scope.vm = {};

        var vm = $scope.vm;
        
        var entityName = $state.current.params.entityName;

        var apiUrl = apiHost + entityName;
        var entityMetdata = metadata[entityName];

        vm.show = true;
        vm.entityName = entityName;
        vm.title = entityName;
        vm.form = entityName;
        vm.collections = [];

        vm.formlyFields = formlyMetadata[entityName].fields;

        function initEntity(entityData) {
            vm.formlyModel = entityData;

            for (var i = 0; i < entityMetdata.Collections.length; i++) {
                var collectionMetadata = entityMetdata.Collections[i];

                var collectionTypeMetadata = metadata[collectionMetadata.TypeName];

                var collectionData = entityData && entityData[collectionMetadata.Name];

                if (!collectionData) {
                    collectionData = [];
                }

                var columnDefs = collectionTypeMetadata.Fields.map(function(fieldMetadata) {
                    return {
                        headerName: fieldMetadata.Label,
                        field: fieldMetadata.Name
                    }
                });

                var createRowClickedEventHandler = function(typeName) {
                    return function (gridContext) {

                        var stateName = $state.current.params.parentStateName + '.' + typeName + '.one.edit';

                        var params = angular.copy($stateParams);
                        params[typeName + 'Id'] = gridContext.data.Id;
                       
                        $state.transitionTo(stateName, params);
                        //vm.show = false;
                    };
                }

                var createAddEntityEventHandler = function(typeName) {
                    return function() {
                        var stateName = $state.current.params.parentStateName + '.' + typeName + '.add';
                        var params = angular.copy($stateParams);
                        
                        $state.transitionTo(stateName, params);
                        //vm.show = false;
                    }
                }

                vm.collections.push({
                    collectionName: collectionMetadata.Name,
                    oneEntityName: collectionMetadata.TypeLabel,
                    addEntityClicked: createAddEntityEventHandler(collectionMetadata.TypeName),
                    gridOptions: {
                        columnDefs: columnDefs,
                        rowData: collectionData,
                        onRowClicked: createRowClickedEventHandler(collectionMetadata.TypeName)
                    }
                });
            }

            vm.ready = true;
        }

        var getEntityData = function() {
            if ($state.params.edit) {
                if ($state.params.data) {
                    initEntity($state.params.data);
                } else {

                    var id = $state.params[entityName + 'Id'];

                    $http
                        .get(apiUrl + '/' + id)
                        .then(function(response) {
                            initEntity(response.data);
                        });
                }
            } else {
                initEntity(null);
            }
        }

        function goToParentState() {
            // $previousState.go() - returns to previous state, not to parent state

            var stateName = $state.current.params.stateToReturnTo;
            var params = angular.copy($stateParams);
            $state.transitionTo(stateName, params);
        }

        vm.submit = function () {
            var entity = vm.formlyModel;
            if ($state.params.edit) {
                $http
                    .put(apiUrl, entity)
                    .then(goToParentState);
            } else if ($state.params.add) {

                if ($state.params.parentEntityName) {
                    var parentEntityIdName = $state.params.parentEntityName + 'Id';
                    var parentEntityId = $stateParams[parentEntityIdName];
                    entity[parentEntityIdName] = parentEntityId;
                }
                
                $http
                    .post(apiUrl, entity)
                    .then(goToParentState);
            }
        }

        vm.delete = function () {
            $http
                .delete(apiUrl + '/' + vm.formlyModel.Id)
                .then(goToParentState);
        }

        vm.cancel = goToParentState;
        
        activate();

        function activate() {
            getEntityData();
            common.activateController([], controllerId)
                .then(function () { log('Activated ' + entityName + ' View'); });
        }
    }
})();