﻿(function () {
    'use strict';

    function getStaticStates() {

        return [
            {
                url: '/',
                templateUrl: 'app/dashboard/dashboard.html',
                name: 'dashboard',
                settings: {
                    nav: 1,
                    content: '<i class="fa fa-dashboard"></i> Dashboard'
                }
            },
            //{
            //    url: '/Other',
            //    name: 'Other',
            //    templateUrl: 'app/other/other.html',
            //    settings: {
            //        nav: 2,
            //        content: '<i class="fa fa-lock"></i> Other'
            //    }
            //}
        ];
    }

    angular.module('app').provider('states', function () {
        
        var self = this;

        var states;

        self.config = function (metadataFromServer) {

            var rootEntities = getRootEntities(metadataFromServer);

            var staticStates = getStaticStates();

            var counter = staticStates.length;

            states = angular.copy(staticStates);

            for (var i = 0; i < rootEntities.length; i++) {
                var entityMetadata = rootEntities[i];
                var entityName = entityMetadata.Name;
                counter++;

                var listStateName = entityName + 'list';
                var oneEntityStateName = entityName + '.one';

                states.push({
                    url: '/' + entityName + 'List',
                    name: listStateName,
                    templateUrl: 'app/generic/genericList.html',
                    params: {
                        entityName: entityName
                    },
                    settings: {
                        nav: counter,
                        content: '<i class="fa fa-lock"></i> ' + entityName
                    }
                });

                var parentEntityName = null;
                var parentStateNameWithDot = '';

                createStatesForOneType(states, entityName, parentEntityName, parentStateNameWithDot, listStateName);

                createStatesForChildCollections(states,
                    entityMetadata.Collections,
                    metadataFromServer,
                    entityName,
                    oneEntityStateName,
                    oneEntityStateName + '.edit');
            }
        };

        self.$get = function () {
            return states;
        };

        function getRootEntities(allMetadata) {

            var rootEntities = [];

            angular.forEach(allMetadata, function (entityMetadata) {
                if (entityMetadata.IsAggregateRoot) {
                    rootEntities.push(entityMetadata);
                }
            });

            return rootEntities;
        }

        function createStatesForOneType(states, entityName, parentEntityName, parentStateNameWithDot, stateToReturnTo) {
            function generateGuidUrlPart(entityName) {
                return '/{' + entityName + 'Id' + ':guid}';
            };

            // Abstract base state for all entities of this type
            states.push({
                abstract: true,
                url: '/' + entityName,
                name: parentStateNameWithDot + entityName,
                template: '<ui-view/>' // Note: abstract still needs a ui-view for its children to populate.
            });

            // Abstract base state for editing entity and all it's children
            states.push({
                abstract: true,
                url: generateGuidUrlPart(entityName),
                name: parentStateNameWithDot + entityName + '.one',
                template: '<ui-view/>' // Note: abstract still needs a ui-view for its children to populate.
            });

            // Edit
            states.push({
                url: '/Edit',
                name: parentStateNameWithDot + entityName + '.one' + '.edit',
                controller: 'genericEntityController',
                templateUrl: 'app/generic/genericEntity.html',
                params: {
                    edit: true,
                    entityName: entityName,
                    stateToReturnTo: stateToReturnTo,
                    parentEntityName: parentEntityName,
                    parentStateName: parentStateNameWithDot + entityName + '.one',
                    data: {
                        value: null
                    }
                }
            });

            // Add
            states.push({
                url: '/Add',
                name: parentStateNameWithDot + entityName + '.add',
                controller: 'genericEntityController',
                templateUrl: 'app/generic/genericEntity.html',
                params: {
                    add: true,
                    entityName: entityName,
                    stateToReturnTo: stateToReturnTo,
                    parentEntityName: parentEntityName,
                    data: {
                        value: null
                    }
                }
            });
        }

        function createStatesForChildCollections(states, collectoins, metadataFromServer, parentEntityName, parentStateName, stateToReturnTo) {
            for (var j = 0; j < collectoins.length; j++) {

                var collectionMetadata = collectoins[j];

                var entityName = collectionMetadata.TypeName;

                var parentStateNameWithDot = parentStateName + '.';

                createStatesForOneType(states, entityName, parentEntityName, parentStateNameWithDot, stateToReturnTo);

                var fullMetadataForType = metadataFromServer[entityName];

                if (fullMetadataForType.Collections.length > 0) {

                    createStatesForChildCollections(states,
                        fullMetadataForType.Collections,
                        metadataFromServer,
                        entityName,
                        parentStateNameWithDot + entityName + '.one',
                        parentStateNameWithDot + entityName + '.one.edit');
                }
            }
        }

    });
})();