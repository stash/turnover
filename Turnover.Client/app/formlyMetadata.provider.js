(function () {
    'use strict';

    angular.module('app').provider('formlyMetadata', function () {
        
        var self = this;

        var formlyMetadata = {};

        self.config = function(metadataFromServer) {
            angular.forEach(metadataFromServer, function (entityMetadata) {
                formlyMetadata[entityMetadata.Name] = {
                    fields: entityMetadata.Fields.map(convertFieldMetadataToFormly),
                    collections: entityMetadata.Collections.map(convertCollectionMetadataToFormly)
                }
            });
        };

        self.$get = function() {
            return formlyMetadata;
        };

        function convertFieldMetadataToFormly(metadata) {
            return {
                key: metadata.Name,
                type: 'input',
                templateOptions: {
                    label: metadata.Label,
                    placeholder: metadata.Label,
                    type: null,
                    minlength: metadata.MinLength,
                    maxlength: metadata.MaxLength,
                    required: metadata.Required
                }
            };
        }

        function convertCollectionMetadataToFormly(metadata) {
            return {
                key: metadata.Name,
                type: 'input',
                templateOptions: {
                    label: metadata.Label,
                    placeholder: metadata.Label,
                    type: null,
                    minlength: metadata.MinLength,
                    maxlength: metadata.MaxLength,
                    required: metadata.Required
                }
            };
        }
    });
})();