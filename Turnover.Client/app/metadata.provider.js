(function () {
    'use strict';

    angular.module('app').provider('metadata', function() {
        
        var self = this;

        var metadata;

        self.config = function(metadataFromServer) {
            metadata = metadataFromServer;
        };

        self.$get = function() {
            return metadata;
        };
    });
})();