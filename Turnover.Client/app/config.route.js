﻿(function () {
    'use strict';

    var apiHost = 'http://localhost:6293/api/';

    var app = angular.module('app');

    app.constant('apiHost', apiHost);

    angular.element(document).ready(function () {
        $.get(apiHost + 'metadata', function (data) {
            
            app
                .config(['$urlMatcherFactoryProvider', initGuidParamType])
                .config(['$stateProvider', '$urlRouterProvider', 'metadataProvider', 'formlyMetadataProvider', 'statesProvider', initConfig]);

            function initGuidParamType($urlMatcherFactoryProvider) {
                var GUID_REGEXP = /^[a-f\d]{8}-([a-f\d]{4}-){3}[a-f\d]{12}$/i;
                $urlMatcherFactoryProvider.type('guid', {
                    encode: angular.identity,
                    decode: angular.identity,
                    is: function(item) {
                        return GUID_REGEXP.test(item);
                    }
                });
            }

            function initConfig($stateProvider, $urlRouterProvider, metadataProvider, formlyMetadataProvider, statesProvider) {
                metadataProvider.config(data);
                formlyMetadataProvider.config(data);
                statesProvider.config(data);

                var states = statesProvider.$get();

                $urlRouterProvider.otherwise('/');

                states.forEach(function (r) {
                    $stateProvider.state(r.name, r);
                });
            }

            angular.bootstrap(document, ['app']);
        });
    });
   
})();